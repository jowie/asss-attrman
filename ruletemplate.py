import re
import random
import asss

cfg = asss.get_interface(asss.I_CONFIG)
chat = asss.get_interface(asss.I_CHAT)
attrman = asss.get_interface(asss.I_ATTRMAN)
attrmancset = asss.get_interface(asss.I_ATTRMANCSET)

class RuleTemplate:
    def __init__(self, arena):
        self.arena = arena
        self.activeAttrSetter = 0
        self.ref_ruletemplateCommand = None

    def attach(self):
        self.ref_ruletemplateCommand = asss.add_command('ruletemplate', self.ruletemplateCommand, self.arena)
        pass

    def detach(self):
        self.ref_ruletemplateCommand = None
        self.clearActiveTemplate()
        pass

    def ruletemplateCommand(self, cmd, params, player, target):
        """\
Module: <py> ruletemplate
Targets: arena
Args: <template_name>
Activates the given named rule template. A rule template consists of attrman
setting. Each rule template is configured in the arena settings. For
example `?ruletemplate foo` activates the attrman settings found under
ruletemplate_foo:0, ruletemplate_foo:1, ruletemplate_foo:2, and so on.
"""

        name = params
        if name == 'none':
            if self.clearActiveTemplate():
                chat.SendAnyMessage(
                    arenaToPlayerList(self.arena),
                    asss.MSG_FUSCHIA,
                    asss.SOUND_BEEP1,
                    None,
                    'Special rules have been disabled!'
                )
            return

        (pickedName, changes) = self.activateRandomTemplate(name)
        if pickedName:
            chat.SendAnyMessage(
                arenaToPlayerList(self.arena),
                asss.MSG_FUSCHIA,
                asss.SOUND_BEEP1,
                None,
                'Special rules have been picked randomly! [%s]' % (pickedName)
            )
            return

        if self.activateTemplate(name):
            chat.SendAnyMessage(
                arenaToPlayerList(self.arena),
                asss.MSG_FUSCHIA,
                asss.SOUND_BEEP1,
                None,
                'Special rules are now in effect! [%s]' % (name)
            )
            return

        chat.SendCmdMessage(player, "Unknown template name: %s" % (name))

    def activateRandomTemplate(self, groupName):
        names = cfg.GetStr(self.arena.cfg, "ruletemplate", groupName)
        if not names:
            return '', 0

        names = re.split('[\s,;]+', names)
        if not names:
            return '', 0

        name = random.choice(names)
        return name, self.activateTemplate(name)

    def activateTemplate(self, name):
        self.clearActiveTemplate()
        settings = 0

        attrman.Lock()
        try:
            self.activeAttrSetter = attrman.RegisterSetter()

            section = "ruletemplate_" + name
            for n in xrange(0, 1000):
                setting = cfg.GetStr(self.arena.cfg, section, str(n))
                if not setting:
                    break

                (success, attribute, value, ship, operation) = attrman.parseAttributeString(setting)
                if success:
                    if operation == asss.ATTRMAN_OP_ABSOLUTE:
                        attrman.SetValue(self.arena, self.activeAttrSetter, attribute, False, ship, value)
                        settings += 1
                    elif operation == asss.ATTRMAN_OP_ADDITION:
                        attrman.SetValue(self.arena, self.activeAttrSetter, attribute, True, ship, value)
                        settings += 1

        finally:
            attrman.UnLock()
        
        if settings > 0:
                attrmancset.SendUpdatesImmediately(self.arena)
        
        return settings

    def clearActiveTemplate(self):
        if not self.activeAttrSetter:
            return False

        attrman.Lock()
        try:
            attrman.UnregisterSetter(self.activeAttrSetter)
            self.activeAttrSetter = 0
        finally:
            attrman.UnLock()

        return True

def arenaToPlayerList(arena):
    def each(player):
        if player.arena == arena:
            playerList.append(player)

    playerList = asss.PlayerListType()
    asss.for_each_player(each)
    return playerList

# called by pymod because it has a magic name
def mm_attach(arena):
    arena.ruletemplate = RuleTemplate(arena)
    arena.ruletemplate.attach()


# called by pymod because it has a magic name
def mm_detach(arena):
    if arena.ruletemplate:
        arena.ruletemplate.detach()

    delattr(arena, 'ruletemplate')