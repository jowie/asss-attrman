// by JoWie & BaK-

#include <stdio.h>


#include "asss.h"
#include "attrman.h"

static Imodman *mm;
static Igame* game;
static Iconfig* cfg;
static Inet* net;
static Iplayerdata* pd;
static Iattrman *attrman;
static Ilogman *lm;

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

#define SHIPS (8)
#define SETTINGS (6)

/* cfghelp: All:InitialMultifire, arena, int, def: 0, range: 0-1
 * Multifire upon spawn */

/* cfghelp: All:InitialProx, arena, int, def: 0, range: 0-1
 * Prox upon spawn */

/* cfghelp: All:InitialSuper, arena, int, def: 0, range: 0-1
 * Temporary super upon spawn */

 /* cfghelp: All:InitialShield, arena, int, def: 0, range: 0-1
 * Temporary shields upon spawn */

 /* cfghelp: All:InitialShrapnel, arena, int, def: 0, range: 0-31
 * one or more shrapnel prizes upon spawn */

 /* cfghelp: All:InitialBounce, arena, int, def: 0, range: 0-1
 * Initial bouncing bullets upon spawn */


static const char* settings[] = { "InitialMultifire", "InitialProx", "InitialSuper", "InitialShield", "InitialShrapnel", "InitialBounce" };
static int prizes[] =           { PRIZE_MULTIFIRE   , PRIZE_PROX   , PRIZE_SUPER   , PRIZE_SHIELD   , PRIZE_SHRAP       , PRIZE_BOUNCE   };

static const char* attrmanNames[SETTINGS] =
{
	"cset::InitialMultifire",
	"cset::InitialProx",
	"cset::InitialSuper",
	"cset::InitialShield",
	"cset::InitialShrapnel",
	"cset::InitialBounce",
};

typedef struct InitialPlayerData
{
	u8 settings[SHIPS][SETTINGS]; // 8 ships, 6 initial settings

} InitialPlayerData;

static int pkey = -1;
static AttrmanSetter configSetter;

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<attrman> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static void ArenaAction(Arena *arena, int action)
{
	int ship, setting;
	Target tgtArena;

	if (!arena) return;
	tgtArena.type = T_ARENA;
	tgtArena.u.arena = arena;

        if (action == AA_CREATE || action == AA_CONFCHANGED)
        {
        	attrman->Lock();
                for (ship = 0; ship < SHIPS; ship++)
		{
			for (setting = 0; setting < SETTINGS; setting++)
			{
				attrman->SetValue(&tgtArena, configSetter, attrmanNames[setting], true, ship, cfg->GetInt(arena->cfg, cfg->SHIP_NAMES[ship], settings[setting], 0));
			}
		}
		attrman->UnLock();
        }
}

static void SetValueCB(const Target *scope, const char *attributeName, signed char changedShip, AttrmanSetter setter, ParamAttrman param)
{
	int ship;
	int setting;
	u8 newValue;
        LinkedList players = LL_INITIALIZER;
        Link *l;
        Player *p;
        Target tgtPlayer;
        InitialPlayerData *pdata;
        tgtPlayer.type = T_PLAYER;
        int change;


	ship = param.number / 1000; //integer division; 8005 / 1000 = 8
	setting = param.number - ship * 1000;
	if (changedShip != ATTRMAN_NO_SHIP && ship != changedShip) // does this ship affect this callback?
		return;

	assertlm(ship >= 0 && ship < SHIPS);
	assertlm(setting >= 0 && setting < SETTINGS);

	pd->Lock();
        pd->TargetToSet(scope, &players);
        for (l = LLGetHead(&players); l; l = l->next)
        {
        	p = l->data;
        	if (!p || !IS_STANDARD(p)) continue;

		tgtPlayer.u.p = p;
                pdata = PPDATA(p, pkey);

		attrman->Lock();
        	newValue = attrman->GetTotalValue(&tgtPlayer, attributeName, ship, NULL);
        	attrman->UnLock();
        	CLIP(newValue, 0, 255);


        	if (pdata->settings[ship][setting] != newValue && p->p_ship == ship) //prize it
        	{
        		if (prizes[setting] == PRIZE_SHRAP) //prizes that have count
        		{
        			change = newValue - pdata->settings[ship][setting];
        			if (change < 0)
        				game->GivePrize(&tgtPlayer, -prizes[setting], -change);
        			else
        				game->GivePrize(&tgtPlayer, prizes[setting], change);
        		}
        		else
        		{
        			if (pdata->settings[ship][setting] <= 0 && newValue > 0)
        			{
        				game->GivePrize(&tgtPlayer, prizes[setting], 1);
        			}
        			else if (pdata->settings[ship][setting] > 0 && newValue <= 0)
        			{
        				game->GivePrize(&tgtPlayer, -prizes[setting], 1);
        			}
        		}



        	}
        	pdata->settings[ship][setting] = newValue;
        }
        pd->Unlock();
	LLEmpty(&players);
}

static void updatePlayerSettings(Player *p)
{
	int ship, setting;
	InitialPlayerData *pdata;
	Target playerScope;
	
	
	assertlm(p);
	if (!p->arena) return;
	playerScope.type = T_PLAYER;
	playerScope.u.p = p;

	pdata = PPDATA(p, pkey);

	attrman->Lock();
	for (ship = 0; ship < SHIPS; ship++)
	{
		for (setting = 0; setting < SETTINGS; setting++)
		{
			pdata->settings[ship][setting] = attrman->GetTotalValue(&playerScope, attrmanNames[setting], ship, NULL);
		}
	}
	attrman->UnLock();
}

static void givePrizes(Player* p)
{
	InitialPlayerData *pdata = PPDATA(p, pkey);
	if (p->p_ship < SHIPS)
	{
		Target t;
		int x;

		t.type = T_PLAYER;
		t.u.p = p;

		for (x = 0; x < SETTINGS; ++x)
		{
			u8 val = pdata->settings[p->p_ship][x];

			if (val)
				game->GivePrize(&t, prizes[x], val);
		}
	}
}

static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
	if (newfreq != oldfreq)
		updatePlayerSettings(p);
}

static void SpawnCB(Player *p, int reason)
{
        givePrizes(p);
}

static void PlayerActionCB(Player *p, int action, Arena *arena)
{
	if (action == PA_ENTERGAME)
	{
		updatePlayerSettings(p);
	}
}

EXPORT const char info_attrman_initial[] =
	"Attribute Manager Initial Prizes (" ASSSVERSION ", " BUILDDATE ") By JoWie <jowie@welcome-to-the-machine.com>"
	"original by bak <http://forums.minegoboom.com/viewtopic.php?t=7059>";

static void ReleaseInterfaces()
{
	mm->ReleaseInterface(cfg);
	mm->ReleaseInterface(game);
	mm->ReleaseInterface(net);
	mm->ReleaseInterface(pd);
	mm->ReleaseInterface(attrman);
	mm->ReleaseInterface(lm);
}

EXPORT int MM_attrman_initial(int action, Imodman *mm_, Arena *arena)
{
	int ship, setting;

	if (action == MM_LOAD)
	{
		mm = mm_;

		game = mm->GetInterface(I_GAME, ALLARENAS);
		cfg = mm->GetInterface(I_CONFIG, ALLARENAS);
		net = mm->GetInterface(I_NET, ALLARENAS);
		pd = mm->GetInterface(I_PLAYERDATA, ALLARENAS);
		attrman = mm->GetInterface(I_ATTRMAN, ALLARENAS);
		lm = mm->GetInterface(I_LOGMAN, ALLARENAS);

		if (!game || !cfg || !net || !pd || !attrman || !lm)
		{
			ReleaseInterfaces();
			return MM_FAIL;
		}
		pkey = pd->AllocatePlayerData(sizeof(InitialPlayerData));

		if (pkey == -1)
		{
			ReleaseInterfaces();
			return MM_FAIL;
		}
		mm->RegCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, ALLARENAS);
		mm->RegCallback(CB_PLAYERACTION, PlayerActionCB, ALLARENAS);
		mm->RegCallback(CB_SPAWN, SpawnCB, ALLARENAS);

		attrman->Lock();
		configSetter = attrman->RegisterSetter();

		for (ship = 0; ship < SHIPS; ship++)
		{
			for (setting = 0; setting < SETTINGS; setting++)
			{
				// Add a callback per ship; (in the old version of attrman there was no support for ships)
				// There is nothing to gain by refactoring this now 
				attrman->RegisterCallback(attrmanNames[setting], SetValueCB, (ParamAttrman){(long) (ship * 1000 + setting), 0});
			}
		}
		attrman->UnLock();



		return MM_OK;
	}
	else if (action == MM_UNLOAD)
	{
		attrman->Lock();
		attrman->UnregisterSetter(configSetter);

		for (ship = 0; ship < SHIPS; ship++)
		{
			for (setting = 0; setting < SETTINGS; setting++)
			{
				attrman->UnregisterCallback(attrmanNames[setting], SetValueCB);
			}
		}
		attrman->UnLock();

		mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);
		mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, ALLARENAS);
		mm->UnregCallback(CB_PLAYERACTION, PlayerActionCB, ALLARENAS);
		mm->UnregCallback(CB_SPAWN, SpawnCB, ALLARENAS);

		pd->FreePlayerData(pkey);

		ReleaseInterfaces();

		return MM_OK;
	}

	return MM_FAIL;
}
