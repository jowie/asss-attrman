#ifndef __ATTRMAN_CSET_H
#define __ATTRMAN_CSET_H

/* pyinclude: attrman/attrman_cset.h */

#define I_ATTRMANCSET "attrmancset-1"

#define CB_SENDCLIENTSETTINGS "attrman_cset_sendclientsettings"
typedef void (*SendClientSettingsFunc)(Player *p);

typedef struct Iattrmancset
{
	INTERFACE_HEAD_DECL
	/* pyint: use */

	void (*SendUpdatesImmediately)(const Target *scope);
	/* pyint: target -> void */
} Iattrmancset;

#endif

