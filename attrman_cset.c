/**
ASSS Attribute Manager Clientset Bridge
jowie@welcome-to-the-machine.com
Copyright (c) 2009-2011  Joris v/d Wel

This file is part of ASSS Attribute Manager

   ASSS Attribute Manager is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Attribute Manager is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with ASSS Attribute Manager.  If not, see <http://www.gnu.org/licenses/>.
  
   In addition, the following supplemental terms apply, based on section 7 of the
   GNU Affero General Public License (version 3):
   a) Preservation of all legal notices and author attributions
   b) Prohibition of misrepresentation of the origin of this material, and
      modified versions are required to be marked in reasonable ways as
      different from the original version

*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "asss.h"
#include "attrman.h"
#include "attrman_cset.h"
#include "clientset.h"
#include "packets/clientset.h"

#ifdef NDEBUG
#define assertlm(x) ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __func__, __LINE__)
#endif

// How often, per player, to send a settings packet
#define SENDCLIENTSETTINGS_DELAY (100)

// How often to pick a player who needs an update
#define SENDCLIENTSETTINGS_CHECKDELAY (4)

struct attributeData
{
        override_key_t key;
        const i32 min;
        const i32 max;
        const i8 ship : 7;
        const i8 funky : 1;
        const char * const attribute; // attrman
        const char * const setting; // arena setting
};

static struct attributeData attributes[] = {

#define X(SECTION, KEY, MIN, MAX, FUNKY) \
{0, MIN, MAX, -1, FUNKY, "cset::" #SECTION ":" #KEY, #SECTION ":" #KEY},

#define XS(SHIPNO, SECTION, KEY, MIN, MAX) \
{0, MIN, MAX, SHIPNO, 0, "cset::" #KEY, #SECTION ":" #KEY},

#include "attrman_clientset.def"

};

static Imodman     *mm;
static Iattrman    *attrman;
static Iarenaman   *arenaman;
static Iplayerdata *pd;
static Iclientset  *cset;
static Ilogman     *lm;
static Iconfig     *cfg;
static Imainloop   *ml; 


static struct playerData*       GetPlayerData           (Player *p);
static void                     SetValueCB              (const Target *scope, char const *attributeName, signed char ship, AttrmanSetter setter, ParamAttrman param);
static bool                     PlayerOverride          (Player *p, struct attributeData const *attribute, i32 val, bool set);
static void                     UpdateOverrides         (Player *p);
static void                     RegisterCallbacks       ();
static void                     UnregisterCallbacks     ();
static void                     ShipFreqChangeCB        (Player *p, int newship, int oldship, int newfreq, int oldfreq);
static void                     SendClientSettings      (Player *p);
static int                      SendClientSettingsTimer (void *nothing);

static int playerKey = -1;

void (*OldSendArenaResponse)(Player *p);


static unsigned long attributes_count = sizeof(attributes) / sizeof(struct attributeData); 
#undef X
#undef XS


struct playerData
{
        Arena *cset_arena;      // Arena this player received the last settings packet in
        int cset_freq;          // Freq this player received the last settings packet in
        bool cset_needsUpdate;  // If true, this player needs a new settings packet
        ticks_t cset_lastUpdate;// Time of the last settings update
        struct ClientSettings isSet;
};

static void AssertLM (const char* e, const char* file, const char *function, int line)
{

        if (lm) lm->Log(L_ERROR | L_SYNC, "<attrman_cset> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static struct playerData* GetPlayerData(Player *p)
{
        // p->arena may be NULL in this function
        assertlm(p);
        struct playerData *pdata = PPDATA(p, playerKey);
        assertlm(pdata);

        return pdata;
}

local int get_cs_set(struct ClientSettings *cs, override_key_t key)
{
        int is_signed = (key & 0x80000000u);
        int len = (key >> 16) & 0x7fffu;
        int offset = key & 0xffffu;

        if (len <= 32 && ((offset & 31) + len) <= 32)
        {
                /* easier case: a bunch of bits that fit within a word boundary */
                int wordoff = offset >> 5;
                int bitoff = offset & 31;
                u32 mask = (0xffffffffu >> (32 - len)) << bitoff;

                u32 value = (((u32*)cs)[wordoff] & mask) >> bitoff;
                if (is_signed && (value & (1 << (len - 1))))
                {
                        value |= (0xffffffffu << len); // do sign extension
                }
                return (int)value;
        }
        
        return 0;
}

local void mark_cs_set(struct ClientSettings *cs, override_key_t key, int set)
{
        int len = (key >> 16) & 0x7fffu;
        int offset = key & 0xffffu;
        
        set = set ? 1 : 0;
        
        if (offset < 8)
                return;

        if (len <= 32 && ((offset & 31) + len) <= 32)
        {
                int wordoff = offset >> 5;
                int bitoff = offset & 31;
                u32 mask = (0xffffffffu >> (32 - len)) << bitoff;
                
                ((u32*)cs)[wordoff] &= ~mask;
                ((u32*)cs)[wordoff] |= (set << bitoff) & mask;
        }
        else
        {
                lm->Log(L_WARN, "<clientset> illegal override key: %x", key);
        }
}

// returns true when the value actually changed
static bool PlayerOverride(Player *p, const struct attributeData *attribute, i32 val, bool set)
{
        assertlm(p);
        assertlm(attribute->key);

        i32 oldval;
        struct playerData *pdata = GetPlayerData(p);
        int wasSet = get_cs_set(&pdata->isSet, attribute->key);
        mark_cs_set(&pdata->isSet, attribute->key, set);

        if (!set)
        {
                if (wasSet)
                {
                        cset->PlayerUnoverride(p, attribute->key);
                }
                return wasSet;
        }

        // Some special stuff
        if (attribute->funky)
        {
                val *= 1000;
        }
        
        if (cset->GetPlayerOverride(p, attribute->key, &oldval))
        {
                cset->PlayerOverride(p, attribute->key, val);

                // PlayerOverride may sanitize our value, so make sure we 
                // compare with the values clientset actually has                
                if (!cset->GetPlayerOverride(p, attribute->key, &val))
                {
                        assertlm(false);
                        return false;        
                }
                
                return val != oldval;
        }
        
        // Player has not received a settings packet before...
        cset->PlayerOverride(p, attribute->key, val);
        return true;
}

static i32 GetPlayerValue(Player *p, const struct attributeData *attribute, bool *isSet)
{
        i32 val;
        int hasAbsoluteValue;
        Target playerScope;
        
        playerScope.type = T_PLAYER;
        playerScope.u.p = p;
        
        attrman->Lock();
        // attributes that have their own ship section (such as Warbird:BulletFireDelay), do not use the current ship of the player
        val = attrman->GetTotalValue(&playerScope, attribute->attribute, (attribute->ship < 0 ? p->p_ship : attribute->ship), &hasAbsoluteValue);
        attrman->UnLock();

        if (isSet)
        {
                *isSet = hasAbsoluteValue || val != 0;
        }

        if (!hasAbsoluteValue && p->arena)
        {
                val += cfg->GetInt(p->arena->cfg, attribute->setting, NULL, 0); // undocumented feature
        }
        
        return val;
}

static void SetValueCB(const Target *scope, const char *attributeName, signed char ship, AttrmanSetter setter, ParamAttrman param)
{
        // Update the overrides for the players in the scope for this attribute

        assertlm(param.pointer);
        LinkedList players = LL_INITIALIZER;
        Link *l;
        const struct attributeData *attribute = param.pointer;

        pd->Lock();
        pd->TargetToSet(scope, &players);

        for (l = LLGetHead(&players); l; l = l->next)
        {
                Player *p = l->data;
                struct playerData *pdata;
                bool isSet;
                i32 val;
                
                if (!p || !IS_STANDARD(p)) continue;
                if (ship != ATTRMAN_NO_SHIP && p->p_ship != ship) continue;
                pdata = GetPlayerData(p);
                
                val = GetPlayerValue(p, attribute, &isSet);
                CLIP(val, attribute->min, attribute->max);

                if (PlayerOverride(p, attribute, val, isSet))
                {
                        if (!pdata->cset_needsUpdate)
                        {
                                pdata->cset_lastUpdate = current_ticks();
                        }
                        pdata->cset_needsUpdate = true;
                }
        }

        pd->Unlock();

        LLEmpty(&players);
}



static void UpdateOverrides(Player *p)
{
        assertlm(p->arena);
        ticks_t startTime = current_millis();

        struct playerData *pdata = GetPlayerData(p);
        int changedValues = 0;

        for (unsigned a = 0; a < attributes_count; a++)
        {
                struct attributeData const * attribute;
                i32 value;
                bool isSet;
                
                attribute = &attributes[a];
                value = GetPlayerValue(p, attribute, &isSet);
                CLIP(value, attribute->min, attribute->max);
                
                if (PlayerOverride(p, attribute, value, isSet))
                { 
                        changedValues++;
                }
        }
        
        if (changedValues)
        {
                pdata->cset_needsUpdate = true;
                pdata->cset_lastUpdate = current_ticks();
        }

        ticks_t endTime = current_millis();
        lm->LogP(L_DRIVEL, "attrman_cset", p, "UpdateOverrides(); Total Time = %dms; needsUpdate = %d; changedValues = %d", TICK_DIFF(endTime, startTime), pdata->cset_needsUpdate, changedValues);

}

static void UnoverrideEverything(Player *p)
{
        assertlm(p);
        
        for (unsigned a = 0; a < attributes_count; a++)
        {
                cset->PlayerUnoverride(p, attributes[a].key);                
        }
}

static void RegisterCallbacks()
{
        ticks_t startTime = current_millis();
        struct attributeData const * attribute;

        attrman->Lock();

        for (unsigned a = 0; a < attributes_count; a++)
        {
                attribute = &attributes[a];
                attrman->RegisterCallback(attribute->attribute, SetValueCB, (ParamAttrman) {(void*) attribute, 0});
        }

        attrman->UnLock();

        ticks_t endTime = current_millis();
        lm->Log(L_DRIVEL, "<attrman_cset> RegisterCallbacks(); Total Time = %dms", TICK_DIFF(endTime, startTime));
}

static void UnregisterCallbacks()
{
        attrman->Lock();

        for (unsigned a = 0; a < attributes_count; a++)
        {
                attrman->UnregisterCallback(attributes[a].attribute, SetValueCB);
        }

        attrman->UnLock();
}

static void SendArenaResponseCB(Player *p) //MIGHT NEED LOCKING
{
        struct playerData *pdata = GetPlayerData(p);

        if (IS_STANDARD(p))
        {
                UpdateOverrides(p);
                pdata->cset_arena = p->arena;
                pdata->cset_freq = p->p_freq;
                pdata->cset_needsUpdate = false;
                pdata->cset_lastUpdate = current_ticks();
        }
        OldSendArenaResponse(p); // This will send a settings packet
}

static void ArenaActionCB(Arena *arena, int action)
{
        Player *p;
        Link *link;
        if (action == AA_CONFCHANGED)
        {
                pd->Lock();
                FOR_EACH_PLAYER(p)
                {
                        if (p->arena == arena && IS_STANDARD(p))
                        {
                                UpdateOverrides(p);
                        }
                }
                pd->Unlock();
        }
}


static void ShipFreqChangeCB(Player *p, int newship, int oldship, int newfreq, int oldfreq)
{
        if (IS_STANDARD(p) && (newfreq != oldfreq || newship != oldship))
        {
                UpdateOverrides(p);
        }
}

static void SendClientSettings(Player *p)
{
        lm->LogP(L_INFO, "attrman_cset", p, "SendClientSettings");
        struct playerData *pdata = GetPlayerData(p);
        if (IS_STANDARD(p) && p->arena)
        {
                pdata->cset_arena = p->arena;
                pdata->cset_freq = p->p_freq;
                pdata->cset_needsUpdate = false;
                pdata->cset_lastUpdate = current_ticks();
        }

        cset->SendClientSettings(p);
        DO_CBS(CB_SENDCLIENTSETTINGS, p->arena, SendClientSettingsFunc, (p));
}

static int SendClientSettingsTimer(void *nothing)
{
        ticks_t now = current_ticks();

        int longestWithoutUpdate_ticks = 0;
        Player *longestWithoutUpdate = NULL;

        int diff = 0;
        Player *p;
        Link *link;
        struct playerData *pdata;

        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                if (!IS_STANDARD(p)) continue;
                pdata = GetPlayerData(p);
                diff = TICK_DIFF(now, pdata->cset_lastUpdate);

                if (pdata->cset_needsUpdate && longestWithoutUpdate_ticks < diff)
                {
                        longestWithoutUpdate_ticks = diff;
                        longestWithoutUpdate = p;
                }
        }
        pd->Unlock();

        if (longestWithoutUpdate && longestWithoutUpdate_ticks >= SENDCLIENTSETTINGS_DELAY)
        {
                SendClientSettings(longestWithoutUpdate);
        }

        return 1;
}

void SendUpdatesImmediately(const Target *scope)
{
        LinkedList players = LL_INITIALIZER;

        pd->Lock();
        pd->TargetToSet(scope, &players);
        pd->Unlock();

        for (Link *l = LLGetHead(&players); l; l = l->next)
        {
                Player *p = l->data;
                if (!IS_STANDARD(p)) { continue; }
                SendClientSettings(p);
        }

        LLEmpty(&players);
}

static Iattrmancset attrmancset =
{
        INTERFACE_HEAD_INIT(I_ATTRMANCSET, "ATTRMANCSET")
        SendUpdatesImmediately
};

EXPORT const char info_attrman_cset[] = "Attribute Manager Clientset Bridge (" ASSSVERSION ", " BUILDDATE ") by JoWie <jowie@welcome-to-the-machine.com>\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(attrman);
        mm->ReleaseInterface(arenaman);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(cset);
        mm->ReleaseInterface(lm);
        mm->ReleaseInterface(cfg);
        mm->ReleaseInterface(ml);
}

EXPORT int MM_attrman_cset(int action, Imodman *mm_, Arena *arena)
{
        int a;
        if (action == MM_LOAD)
        {
                mm      = mm_;
                attrman = mm->GetInterface(I_ATTRMAN,     ALLARENAS);
                arenaman= mm->GetInterface(I_ARENAMAN,    ALLARENAS);
                pd      = mm->GetInterface(I_PLAYERDATA,  ALLARENAS);
                cset    = mm->GetInterface(I_CLIENTSET,   ALLARENAS);
                lm      = mm->GetInterface(I_LOGMAN,      ALLARENAS);
                cfg     = mm->GetInterface(I_CONFIG,      ALLARENAS);
                ml      = mm->GetInterface(I_MAINLOOP,    ALLARENAS);

                if (!attrman || !arenaman || !pd || !cset || !lm || !cfg || !ml)
                {
                        printf("<attrman_cset> Missing Interface\n");

                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));
                if (playerKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }
        
                a = -1;

#define X(SECTION, KEY, MIN, MAX, FUNKY) \
        attributes[++a].key = cset->GetOverrideKey(#SECTION, #KEY); \
        assertlm(attributes[a].key);

#define XS(SHIPNO, SECTION, KEY, MIN, MAX) \
        attributes[++a].key = cset->GetOverrideKey(#SECTION, #KEY); \
        assertlm(attributes[a].key);
                
#include "attrman_clientset.def"
#undef X
#undef XS
                RegisterCallbacks();
                OldSendArenaResponse = arenaman->SendArenaResponse;
                arenaman->SendArenaResponse = SendArenaResponseCB;
                mm->RegCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, ALLARENAS);
                mm->RegCallback(CB_ARENAACTION, ArenaActionCB, ALLARENAS);

                ml->SetTimer(SendClientSettingsTimer, SENDCLIENTSETTINGS_DELAY, SENDCLIENTSETTINGS_DELAY, ALLARENAS, ALLARENAS);
                
                mm->RegInterface(&attrmancset, ALLARENAS);

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                if (arenaman->SendArenaResponse != SendArenaResponseCB)
                        return MM_FAIL;
                
                if (mm->UnregInterface(&attrmancset, ALLARENAS))
                        return MM_FAIL;

                ml->ClearTimer(SendClientSettingsTimer, ALLARENAS);

                UnregisterCallbacks();
                arenaman->SendArenaResponse = OldSendArenaResponse;
                mm->UnregCallback(CB_SHIPFREQCHANGE, ShipFreqChangeCB, ALLARENAS);
                mm->UnregCallback(CB_ARENAACTION, ArenaActionCB, ALLARENAS);

                Player *p;
                Link *link;

                pd->Lock();
                FOR_EACH_PLAYER(p)
                {
                        if (IS_STANDARD(p))
                                UnoverrideEverything(p);
                }
                pd->Unlock();

                ReleaseInterfaces();
                return MM_OK;
        }

        return MM_FAIL;
}
