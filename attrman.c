/**
ASSS Attribute Manager
jowie@welcome-to-the-machine.com
Copyright (c) 2009-2011  Joris v/d Wel

This file is part of ASSS Attribute Manager

   ASSS Attribute Manager is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as published by
   the Free Software Foundation, version 3 of the License.

   ASSS Attribute Manager is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with ASSS Attribute Manager.  If not, see <http://www.gnu.org/licenses/>.
  
   In addition, the following supplemental terms apply, based on section 7 of the
   GNU Affero General Public License (version 3):
   a) Preservation of all legal notices and author attributions
   b) Prohibition of misrepresentation of the origin of this material, and
      modified versions are required to be marked in reasonable ways as
      different from the original version

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>

#include "asss.h"
#include "attrman.h"
#include "pthread.h"

static pthread_mutex_t mtx;

#ifdef NDEBUG
#define assertlm(x)	       ((void)0)
#else
#define assertlm(e) if (!(e)) AssertLM(#e, __FILE__, __FUNCTION__, __LINE__)
#endif

static void                     AssertLM                      (const char* e, const char* file, const char *function, int line);
static struct arenaData *       GetArenaData                  (Arena *arena);
static struct playerData *      GetPlayerData                 (Player *p);
static struct freqData *        GetFreqData                   (Arena *arena, unsigned freq, bool allocFreqData);
static void                     FreeAllData                   (AttrmanSetter setter);
static void                     FreeZoneData                  (AttrmanSetter setter);
static void                     FreeArenaData                 (struct arenaData *adata, AttrmanSetter setter);
static void                     FreeFreqData                  (struct freqData *fdata, AttrmanSetter setter);
static void                     FreePlayerData                (struct playerData *pdata, AttrmanSetter setter);
static HashTable *              GetAttributesTable            (const Target *scope);
static void 			UnsetAll                      (AttrmanSetter setter);
static long                     UnsetValue                    (const Target *scope, AttrmanSetter setter, const char *attributeName, signed char ship);
static void                     SetValue                      (const Target *scope, AttrmanSetter setter, const char *attributeName, bool add, signed char ship, long value);
static long                     GetValue                      (const Target *scope, AttrmanSetter setter, const char *attributeName, bool recursive, signed char ship);
static long                     GetTotalValue                 (const Target *scope, const char *attributeName, bool recursive, signed char ship, bool *hasAbsoluteValue);
static long 			GetTotalValueInt              (const Target *scope, const char *attributeName, signed char ship, int *hasAbsoluteValue);
static long			GetPlayerValue                (Player *p, const char *attributeName, bool *hasAbsoluteValue);
static long			GetPlayerValueInt             (Player *p, const char *attributeName, int *hasAbsoluteValue);
static void 			ReadArenaSettings	      (Arena *arena);
static void 			ReadZoneSettings	      ();
static void                     NewPlayer                     (Player *p, int isNew);
static void                     ArenaAction                   (Arena *arena, int action);
static void                     UnregisterSetter              (AttrmanSetter setter);
static AttrmanSetter            RegisterSetter                ();
static void                     RegisterCallback              (const char *attributeName, SetValueFunc setValueCallback, ParamAttrman param);
static void                     UnregisterCallback            (const char *attributeName, SetValueFunc setValueCallback);
static void                     DoCallback                    (const char *attributeName, const Target *scope, signed char ship, AttrmanSetter setter);
static bool 			parseAttributeString          (const char *str, char *attribute, int maxAttributeSize, long *value, int *ship, ATTRIBUTESTRING_OPERATION *operation);
static void                     ReleaseInterfaces             ();

static Imodman       *mm;
static Ilogman       *lm;
static Iplayerdata   *pd;
static Iarenaman     *aman;
static Ichat         *chat;
static Icmdman       *cmd;
static Iconfig       *cfg;

#define SHIPS (9)
static const char *const shipNames[] = {"warbird","javelin","spider","leviathan","terrier","weasel","lancaster","shark","spectator"};

static inline void Lock()
{
        pthread_mutex_lock(&mtx);
}
static inline void UnLock()
{
        pthread_mutex_unlock(&mtx);
}

static Iattrman attrman =
{
        INTERFACE_HEAD_INIT(I_ATTRMAN, "ATTRMAN")
        Lock,
        UnLock,
        RegisterSetter,
        UnregisterSetter,
        UnsetAll,
        RegisterCallback,
        UnregisterCallback,
        SetValue,
        UnsetValue,
        GetValue,
        GetTotalValueInt,
        GetPlayerValueInt,
        parseAttributeString,
        {0} // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=53119
};

static int arenaKey = -1;
static int playerKey = -1;

static AttrmanSetter nextSetter = 1; //The next setter that will be given when the freeSetters array is empty
static AttrmanSetter *freeSetters = NULL; //array containing setters that may be reused
static unsigned long freeSetters_count = 0;
static unsigned long freeSetters_allocated = 0;
static AttrmanSetter defaultZoneConfigSetter;
static AttrmanSetter defaultArenaConfigSetter;
static AttrmanSetter cmdSetter = ULONG_MAX; //we want this one to have precedence above all others

struct attributeData
{
        long value;
        AttrmanSetter setter;
        bool addition;
        bool ignore; // if this is set, act like this attribute does not exist
        signed char ship;
};

struct playerData
{
	Player *player;
        Arena *arena; //arena these other variables belong to
        HashTable *attributes; //struct attributeData* (free me in CB_NEWPLAYER(p, 0))
};

struct freqData
{
        Arena *arena;
        unsigned freq;
        HashTable *attributes; //struct attributeData*
};

struct arenaData
{
	Arena *arena;
        LinkedList freqs; //struct freqData*
        HashTable *attributes; //struct attributeData*
};

struct setValueCallback
{
        SetValueFunc setValueCallback;
        ParamAttrman param;
};

// ZONE wide
static HashTable *zoneAttributes = NULL; //struct attributeData*
static HashTable *setValueCallbacks = NULL; //struct setValueCallback

static void AssertLM (const char* e, const char* file, const char *function, int line)
{
        if (lm) lm->Log(L_ERROR | L_SYNC, "<attrman> Assertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
        fullsleep(500);
        Error(EXIT_GENERAL, "\nAssertion \"%s\" failed: file \"%s\", line %d, function %s()\n", e, file, line, function);
}

static long safeaddL(signed long num1, signed long num2) 
{
	if (num1 > 0 && num2 > 0 && LONG_MAX - num2 < num1) 
	{
		return LONG_MAX;
	}
	
	if (num1 < 0 && num2 < 0 && LONG_MIN - num2 > num1)
	{
		return LONG_MIN;
	}
	
	return num1 + num2;
}


static int stricmp(const char *str1, const char *str2)
{
	assertlm(str1 && str2);

        for (; *str1 != '\0' && *str2 != '\0' && tolower(*str1) == tolower(*str2);
                str1++, str2++)
                ;
        if (*str1 == '\0')
                return *str2 == '\0'? 0 : -1;
        else if (*str2 == '\0')
                return 1;
        else
                return tolower(*str1) < tolower(*str2)? -1 : 1;
}

static struct arenaData *GetArenaData(Arena *arena)
{
        struct arenaData *adata;
        assertlm(arena);
        assertlm(arenaKey != -1);
        adata = P_ARENA_DATA(arena, arenaKey);
        assertlm(adata);

        if (!adata->attributes)
        {
        	adata->arena = arena;
                LLInit(&adata->freqs);
                adata->attributes = HashAlloc();
        }

        return adata;
}

static struct playerData* GetPlayerData(Player *p)
{
        struct playerData *pdata;
        //p->arena may be NULL in this function
        assertlm(p);
        pdata = PPDATA(p, playerKey);
        assertlm(pdata);

        if (p->arena != pdata->arena)
        {
        	pdata->player = p;
                pdata->arena = p->arena;
                if (pdata->attributes)
                {
                        //free everything in the hash table
                        HashEnum(pdata->attributes, hash_enum_afree, NULL);
			HashFree(pdata->attributes);
		}

                pdata->attributes = HashAlloc();

        }
        return pdata;
}

static struct freqData *GetFreqData(Arena *arena, unsigned freq, bool allocFreqData)
{
        struct arenaData *adata;
        struct freqData *fdata;
        Link *l;

        assertlm(arena);
        assertlm(freq < 10000);
        adata = GetArenaData(arena);

        for (l = LLGetHead(&adata->freqs); l; l = l->next)
        {
                fdata = l->data;

                if (fdata->freq == freq)
                        return fdata;

        }

        // freq data not found after this point
        if (allocFreqData)
        {
                fdata = amalloc(sizeof(struct freqData));
                fdata->arena = arena;
                fdata->freq = freq;

                fdata->attributes = HashAlloc();
                LLAdd(&adata->freqs, fdata);
                return fdata;
        }

        return NULL;
}

struct freeData_HashEnum_closure
{
	AttrmanSetter setter;
	Target cbTgt;
};

/** Setter of 0 means, every setter */
int FreeData_HashEnum(const char *key, void *val, void *clos_)
{
	struct freeData_HashEnum_closure *clos = (struct freeData_HashEnum_closure *) clos_;
        struct attributeData *attrData = val;

        if (!clos->setter)
        {
		// The attribute is not included in gettotalvalue this way :
        	attrData->ignore = true;

        	DoCallback(key, &clos->cbTgt, attrData->ship, attrData->setter);
                afree(attrData);
                return 1;
        }

        if (attrData->setter == clos->setter)
        {
        	attrData->ignore = true;

        	DoCallback(key, &clos->cbTgt, attrData->ship, attrData->setter);
                afree(attrData);
                return 1;
        }

        return 0;
}

/** Free zone, arena, player data . Setter of 0 means free everything */
static void FreeAllData(AttrmanSetter setter)
{
        Link *link;
        Arena *arena;
        Player *p;

        FreeZoneData(setter);

        aman->Lock();
        FOR_EACH_ARENA(arena)
        {
                FreeArenaData(GetArenaData(arena), setter); //also clears freq data
        }
        aman->Unlock();

        pd->Lock();
        FOR_EACH_PLAYER(p)
        {
                FreePlayerData(GetPlayerData(p), setter);
        }
        pd->Unlock();
}


/** Free zone data. Setter of 0 means free everything */
static void FreeZoneData(AttrmanSetter setter)
{
	struct freeData_HashEnum_closure clos;
	clos.cbTgt.type = T_ZONE;
	clos.setter = setter;

        HashEnum(zoneAttributes, FreeData_HashEnum, &clos);
        if (!setter) HashFree(zoneAttributes);
}

/** Free arena data and its freq data */
static void FreeArenaData(struct arenaData *adata, AttrmanSetter setter)
{
        Link *l;
        struct freeData_HashEnum_closure clos;
	clos.cbTgt.type = T_ARENA;
	clos.cbTgt.u.arena = adata->arena;
	clos.setter = setter;

        if (adata->attributes)
        {
                HashEnum(adata->attributes, FreeData_HashEnum, &clos);
                if (!setter)
                {
                        HashFree(adata->attributes);
                        adata->attributes = NULL;
                }
        }

        for (l = LLGetHead(&adata->freqs); l; l = l->next)
        {
                FreeFreqData(l->data, setter);
        }

        if (!setter)
                LLEmpty(&adata->freqs);
}

/** Free freq data */
static void FreeFreqData(struct freqData *fdata, AttrmanSetter setter)
{
        struct freeData_HashEnum_closure clos;
	clos.cbTgt.type = T_FREQ;
	clos.cbTgt.u.freq.freq = fdata->freq;
	clos.cbTgt.u.freq.arena = fdata->arena;
	clos.setter = setter;

        HashEnum(fdata->attributes, FreeData_HashEnum, &clos);
        if (!setter)
        {
                HashFree(fdata->attributes);
                fdata->attributes = NULL;
                afree(fdata);
        }

}

/** Free player data*/
static void FreePlayerData(struct playerData *pdata, AttrmanSetter setter)
{
        struct freeData_HashEnum_closure clos;
	clos.cbTgt.type = T_PLAYER;
	clos.cbTgt.u.p = pdata->player;
	clos.setter = setter;

        if (pdata->attributes)
        {
                HashEnum(pdata->attributes, FreeData_HashEnum, &clos);
                if (!setter)
                {
                        HashFree(pdata->attributes);
                        pdata->attributes = NULL;
                }
        }
}

static void NewPlayer(Player *p, int isNew)
{
        if (!isNew) //player struct is about to be deallocated
        {
        	Lock();
                FreePlayerData(GetPlayerData(p), 0);
                UnLock();
        }
}

static void ArenaAction(Arena *arena, int action)
{
        if (action == AA_PRECREATE) //before modules are attached
        {
        	Lock();
        	GetArenaData(arena); //initialize arenadata
                UnLock();
        }
        else if (action == AA_POSTDESTROY) //arena struct is about to be deallocated, all modules are already detached from this arena
        {
        	Lock();
                FreeArenaData(GetArenaData(arena), 0);
                UnLock();
        }
        else if (action == AA_CREATE || action == AA_CONFCHANGED)
        {
        	Lock();
        	ReadArenaSettings(arena);
        	UnLock();
        }
}

static void GlobalConfigChanged()
{
	Lock();
	ReadZoneSettings();
	UnLock();
}

static void ReadArenaSettings(Arena *arena)
{
        assertlm(arena);

        char buf[256];
        int a;
        const char *s;
        Target scope;
        scope.type = T_ARENA;
        scope.u.arena = arena;
        long value;
        int ship;
        ATTRIBUTESTRING_OPERATION operation;

        ConfigHandle ch = arena->cfg;

        FreeArenaData(GetArenaData(arena), defaultArenaConfigSetter); // remove the old ones

        for (a = 0; true; a++)
        {
        	snprintf(buf, sizeof(buf), "%d", a);
        	operation = ATTRMAN_OP_ERROR;

        	s = cfg->GetStr(ch, "attrman", buf);
	        if (!s) break; // reached end of the list

	        if (parseAttributeString(s, buf, sizeof(buf), &value, &ship, &operation) && (operation == ATTRMAN_OP_ABSOLUTE || operation == ATTRMAN_OP_ADDITION) )
	        {
	        	if (operation == ATTRMAN_OP_ABSOLUTE)
	        	{
	        		SetValue(&scope, defaultArenaConfigSetter, buf, false, ship, value);
	        	}
	        	else if (operation == ATTRMAN_OP_ADDITION)
	        	{
	        		SetValue(&scope, defaultArenaConfigSetter, buf, true, ship, value);
	        	}
	        }
	        else
	        {
	        	lm->LogA(L_ERROR, "attrman", arena, "Incorrect syntax for setting attrman:%d", a);
	        }
        }
}


static void ReadZoneSettings()
{
        char buf[256];
        int a;
        const char *s;
        Target scope;
        scope.type = T_ZONE;
        long value;
        int ship;
        ATTRIBUTESTRING_OPERATION operation;

        ConfigHandle ch = GLOBAL;

        FreeZoneData(defaultZoneConfigSetter);

        for (a = 0; true; a++)
        {
        	snprintf(buf, sizeof(buf), "%d", a);
        	operation = ATTRMAN_OP_ERROR;

        	s = cfg->GetStr(ch, "attrman", buf);
	        if (!s) break; // reached end of the list

	        if (parseAttributeString(s, buf, sizeof(buf), &value, &ship, &operation) && (operation == ATTRMAN_OP_ABSOLUTE || operation == ATTRMAN_OP_ADDITION) )
	        {
	        	if (operation == ATTRMAN_OP_ABSOLUTE)
	        	{
	        		SetValue(&scope, defaultZoneConfigSetter, buf, false, ship, value);
	        	}
	        	else if (operation == ATTRMAN_OP_ADDITION)
	        	{
	        		SetValue(&scope, defaultZoneConfigSetter, buf, true, ship, value);
	        	}
	        }
	        else
	        {
	        	lm->Log(L_ERROR, "<attrman> Incorrect syntax for setting attrman:%d", a);
	        }
        }
}

static AttrmanSetter RegisterSetter()
{
        AttrmanSetter newSetter;
        if (freeSetters_count > 0)
        {
                newSetter = freeSetters[freeSetters_count - 1]; //get the last free setter
                freeSetters[freeSetters_count - 1] = 0; //set that last free setter to 0
                freeSetters_count--; //lower the count

                assertlm(newSetter); //the freeSetters array should never have a setter of 0 (within the count)

                return newSetter;
        }
        else //There are no setters to reuse, create a new one
        {
                return nextSetter++;
        }
}

static void UnregisterSetter(AttrmanSetter setter)
{
        assertlm(setter); //must not be 0, 0 is a special value and means remove everything

        UnsetAll(setter);

        if (!freeSetters)
        {
                freeSetters = amalloc(sizeof(AttrmanSetter) * 2);
                freeSetters_allocated = 2;
                freeSetters_count = 1;
                freeSetters[0] = setter;
        }
        else
        {
                if (freeSetters_count == freeSetters_allocated)
                {
                        freeSetters = arealloc(freeSetters, sizeof(AttrmanSetter) * (freeSetters_allocated * 2));
                        memset(freeSetters + freeSetters_allocated, 0, sizeof(AttrmanSetter) * freeSetters_allocated);
                        freeSetters_allocated *= 2;
                }

                freeSetters[freeSetters_count++] = setter;
        }

}

static void UnsetAll(AttrmanSetter setter)
{
	assertlm(setter); //must not be 0, 0 is a special value and means remove everything
        FreeAllData(setter);
}

static void RegisterCallback(const char *attributeName, SetValueFunc setValueCallback, ParamAttrman param)
{
        struct setValueCallback *callback = amalloc(sizeof(struct setValueCallback));
        callback->setValueCallback = setValueCallback;
        callback->param = param;

        HashAdd(setValueCallbacks, attributeName, callback);
}

static void UnregisterCallback(const char *attributeName, SetValueFunc setValueCallback)
{
        LinkedList callbacks = LL_INITIALIZER;
        struct setValueCallback *callback;
        Link *l;

        HashGetAppend(setValueCallbacks, attributeName, &callbacks);

        for (l = LLGetHead(&callbacks); l; l = l->next)
        {
                callback = l->data;
                if (callback->setValueCallback == setValueCallback)
                {
                        HashRemove(setValueCallbacks, attributeName, callback);
                        afree(callback);
                }
        }
        LLEmpty(&callbacks);
}

static void DoCallback(const char *attributeName, const Target *scope, signed char ship, AttrmanSetter setter)
{
        LinkedList callbacks = LL_INITIALIZER;
        Link *l;
        struct setValueCallback *callback;

        assertlm(setValueCallbacks);

        HashGetAppend(setValueCallbacks, attributeName, &callbacks);

        for (l = LLGetHead(&callbacks); l; l = l->next)
        {
                callback = l->data;
                callback->setValueCallback(scope, attributeName, ship, setter, callback->param);
        }
}

/** Returns the hashtable associated with the scope */
static HashTable *GetAttributesTable(const Target *scope)
{
        struct arenaData *adata;
        struct freqData *fdata;
        struct playerData *pdata;

        assertlm(scope);

        switch (scope->type)
        {
                case T_ZONE:
                        return zoneAttributes;

                case T_ARENA:

                        adata = GetArenaData(scope->u.arena);
                        return adata->attributes;

                case T_FREQ:

                        fdata = GetFreqData(scope->u.freq.arena, scope->u.freq.freq, true);
                        return fdata->attributes;

                case T_PLAYER:

                        pdata = GetPlayerData(scope->u.p);
                        return pdata->attributes;

                default: //not supported
                        assertlm(false);
                        return NULL;

        }

        assertlm(false);
        return NULL;
}


static void SetValue(const Target *scope, AttrmanSetter setter, const char *attributeName, bool addition, signed char ship, long value)
{
        HashTable *attributesTable;
        LinkedList foundAttributes = LL_INITIALIZER;
        Link *l;
        struct attributeData *attrData = NULL;

        assertlm(scope);
        assertlm(setter); //never 0
        assertlm(attributeName); //can not pass NULL
        assertlm(scope->type == T_ZONE || scope->type == T_ARENA || scope->type == T_FREQ || scope->type == T_PLAYER);


        attributesTable = GetAttributesTable(scope); //set depending on the scope type
        assertlm(attributesTable);

        HashGetAppend(attributesTable, attributeName, &foundAttributes);


        for (l = LLGetHead(&foundAttributes); l; l = l->next)
        {
                attrData = l->data;
                if (attrData->setter == setter && attrData->ship == ship)
                {
                        break; //attribute found, do not set it to NULL
                }
                attrData = NULL;
        }


        if (attrData) //attribute found, just set the value
        {
                attrData->value = value;
                attrData->addition = addition;
                attrData->ship = ship;
        }
        else //not found, allocate a new attribute first
        {
                attrData = amalloc(sizeof(struct attributeData));
                attrData->setter = setter;
                attrData->value = value;
                attrData->ship = ship;
                attrData->addition = addition;
                HashAdd(attributesTable, attributeName, attrData);
        }


        LLEmpty(&foundAttributes);
        DoCallback(attributeName, scope, ship, setter);
}

static long UnsetValue(const Target *scope, AttrmanSetter setter, const char *attributeName, signed char ship)
{
        Link *l;
        struct attributeData *attrData = NULL;
        int a = 0;
        LinkedList foundAttributes = LL_INITIALIZER;
        long oldValue = 0;
        HashTable *attributesTable;

        assertlm(scope);
        assertlm(setter); //never 0
        assertlm(attributeName); //can not pass NULL
        assertlm(scope->type == T_ZONE || scope->type == T_ARENA || scope->type == T_FREQ || scope->type == T_PLAYER);


        attributesTable = GetAttributesTable(scope); //set depending on the scope type
        assertlm(attributesTable);

        HashGetAppend(attributesTable, attributeName, &foundAttributes);

        for (l = LLGetHead(&foundAttributes); l; l = l->next)
        {
                attrData = l->data;
                if (attrData->setter == setter && attrData->ship == ship)
                {
                        oldValue = attrData->value;
                        //attribute with correct setter and name found, remove it
                        HashRemove(attributesTable, attributeName, attrData);
                        afree(attrData);
                        attrData = NULL;
                        a++;
                        //keep looking after this, even though this should never happen it is possible there are multiple attributes with the same setter
                }
        }

        LLEmpty(&foundAttributes);
        assertlm(a < 2); // There are multiple attributes with the same setter!


        DoCallback(attributeName, scope, ship, setter);
        return oldValue;
}

static long GetValue(const Target *scope, AttrmanSetter setter, const char *attributeName, bool recursive, signed char ship)
{
        struct attributeData *attrData = NULL;
        LinkedList foundAttributes = LL_INITIALIZER;
        Link *l;
        Target scope2;

        assertlm(scope);
        assertlm(attributeName);
        assertlm(scope->type == T_ZONE || scope->type == T_ARENA || scope->type == T_FREQ || scope->type == T_PLAYER);


        HashTable *attributesTable = GetAttributesTable(scope); //set depending on the scope type
        assertlm(attributesTable);


        HashGetAppend(attributesTable, attributeName, &foundAttributes);

        for (l = LLGetHead(&foundAttributes); l; l = l->next)
        {
                attrData = l->data;
                if ((!setter || attrData->setter == setter) &&
		    attrData->ship == ship && !attrData->ignore
		)
                {
                        LLEmpty(&foundAttributes);

                        return attrData->value;
                }
        }

        LLEmpty(&foundAttributes);

        // Attribute not found at this point.

        if (recursive)
        {
                switch (scope->type)
                {
                        case T_ZONE:

                                return 0;

                        case T_ARENA:
                                scope2.type = T_ZONE;

                                return GetValue(&scope2, setter, attributeName, true, ship);

                        case T_FREQ:
                                scope2.type = T_ARENA;
                                scope2.u.arena = scope->u.freq.arena;
                                assertlm(scope2.u.arena);

                                return GetValue(&scope2, setter, attributeName, true, ship);

                        case T_PLAYER:
                                if (!scope->u.p->arena)
                                {

                                        return 0;
                                }

                                scope2.type = T_FREQ;
                                scope2.u.freq.arena = scope->u.p->arena;
                                scope2.u.freq.freq = scope->u.p->p_freq;

                                return GetValue(&scope2, setter, attributeName, true, ship);

                        default:
                                assertlm(false); // Not supported

                                return 0;
                }
        }

        return 0;
}

static long GetPlayerValue(Player *p, const char *attributeName, bool *hasAbsoluteValue)
{
	Target playerScope;
	playerScope.type = T_PLAYER;
	playerScope.u.p = p;
	return GetTotalValue(&playerScope, attributeName, true, p->p_ship, hasAbsoluteValue);
}

static long GetPlayerValueInt(Player *p, const char *attributeName, int *hasAbsoluteValue)
{
	bool bool_hasAbsoluteValue = false; 
	long ret;
	
	ret = GetPlayerValue(p, attributeName, &bool_hasAbsoluteValue);
	if (hasAbsoluteValue)
		*hasAbsoluteValue = bool_hasAbsoluteValue;
	
	return ret;
}

static long GetTotalValueInt(const Target *scope, const char *attributeName, signed char ship, int *hasAbsoluteValue)
{
	bool bool_hasAbsoluteValue = false; 
	long ret;
	
	ret = GetTotalValue(scope, attributeName, true, ship, &bool_hasAbsoluteValue);
	if (hasAbsoluteValue)
		*hasAbsoluteValue = bool_hasAbsoluteValue;
	
	return ret;
}

static long GetTotalValue(const Target *scope, const char *attributeName, bool recursive, signed char ship, bool *hasAbsoluteValue)
{
        long totalValue = 0;
        HashTable *attributesTable;
        Link *l;
        LinkedList foundAttributes = LL_INITIALIZER;
        struct attributeData *attrData;
        Target tmpScope;
	AttrmanSetter setterOfHighestNonAddition = 0;
	long valueOfHighestNonAddition = 0;
	signed char shipOfHighestNonAddition = ATTRMAN_NO_SHIP;
	bool foundAbsoluteValue = false;

	if (hasAbsoluteValue) *hasAbsoluteValue = false;

        assertlm(scope);
        assertlm(attributeName);
        assertlm(scope->type == T_ZONE || scope->type == T_ARENA || scope->type == T_FREQ || scope->type == T_PLAYER);



        if (!recursive)
        {
                attributesTable = GetAttributesTable(scope); //set depending on the scope type
                assertlm(attributesTable);

                HashGetAppend(attributesTable, attributeName, &foundAttributes);

		// Final value is all the attributes with 'addition' set or the value of the absolute with the highest setter

                for (l = LLGetHead(&foundAttributes); l; l = l->next)
                {
                        attrData = l->data;

                        if (attrData->ship != ATTRMAN_NO_SHIP && attrData->ship != ship)
                        	continue;

                        if (attrData->ignore)
                        	continue;

                        if (attrData->addition)
                        {
                        	if (!foundAbsoluteValue && (attrData->ship == ship || attrData->ship == ATTRMAN_NO_SHIP)) // do not bother if we already found an absolute value
                        	{
                        		totalValue = safeaddL(totalValue, attrData->value);
                        	}
                        }
			else
                        {
                        
                        	// a definition with a ship takes precedence over one without a ship; regardless of the setter
				if (shipOfHighestNonAddition == ATTRMAN_NO_SHIP && ship != ATTRMAN_NO_SHIP && attrData->ship == ship)
					setterOfHighestNonAddition = 0;
				
				
				if (attrData->setter >= setterOfHighestNonAddition &&
				    (attrData->ship == ship || (attrData->ship == ATTRMAN_NO_SHIP && shipOfHighestNonAddition == ATTRMAN_NO_SHIP))
				   )
				{
					setterOfHighestNonAddition = attrData->setter;
					valueOfHighestNonAddition = attrData->value;
					shipOfHighestNonAddition = attrData->ship;
					foundAbsoluteValue = true;
				}
			}
                }

                LLEmpty(&foundAttributes);

		if (hasAbsoluteValue)
			*hasAbsoluteValue = foundAbsoluteValue;
		
                return foundAbsoluteValue ? valueOfHighestNonAddition : totalValue;
        }
        else
        {

                switch (scope->type)
                {
                        case T_ZONE:
                        	totalValue = safeaddL(totalValue, GetTotalValue(scope, attributeName, false, ship, &foundAbsoluteValue));
				if (foundAbsoluteValue && hasAbsoluteValue) *hasAbsoluteValue = true;
                                return totalValue;

                        case T_ARENA:
                        	//T_ARENA
                        	totalValue = safeaddL(totalValue, GetTotalValue(scope, attributeName, false, ship, &foundAbsoluteValue));

                        	if (!foundAbsoluteValue)
                        	{
	                                tmpScope.type = T_ZONE;
	                                totalValue = safeaddL(totalValue, GetTotalValue(&tmpScope, attributeName, false, ship, &foundAbsoluteValue));
				}

				if (foundAbsoluteValue && hasAbsoluteValue) *hasAbsoluteValue = true;
                                return totalValue;

                        case T_FREQ:
                                assertlm(scope->u.freq.arena);

				//T_FREQ
				totalValue = safeaddL(totalValue, GetTotalValue(scope, attributeName, false, ship, &foundAbsoluteValue));

				if (!foundAbsoluteValue)
                        	{
					tmpScope.type = T_ARENA;
	                                tmpScope.u.arena = scope->u.freq.arena;
	                                totalValue = safeaddL(totalValue, GetTotalValue(&tmpScope, attributeName, false, ship, &foundAbsoluteValue));
                                }

                                if (!foundAbsoluteValue)
                        	{
	                                tmpScope.type = T_ZONE;
	                                totalValue = safeaddL(totalValue, GetTotalValue(&tmpScope, attributeName, false, ship, &foundAbsoluteValue));
				}

				if (foundAbsoluteValue && hasAbsoluteValue) *hasAbsoluteValue = true;
                                return totalValue;


                        case T_PLAYER:
                        	//T_PLAYER
                        	totalValue = safeaddL(totalValue, GetTotalValue(scope, attributeName, false, ship, &foundAbsoluteValue));


                                if (scope->u.p->arena)
                                {
                                	if (!foundAbsoluteValue)
                        		{
	                                	tmpScope.type = T_FREQ;
	                                        tmpScope.u.freq.arena = scope->u.p->arena;
	                                        tmpScope.u.freq.freq = scope->u.p->p_freq;
	                                        totalValue = safeaddL(totalValue, GetTotalValue(&tmpScope, attributeName, false, ship, &foundAbsoluteValue));
                                        }

	                                if (!foundAbsoluteValue)
	                        	{
	                                        tmpScope.type = T_ARENA;
	                                        tmpScope.u.arena = scope->u.p->arena;
	                                        totalValue = safeaddL(totalValue, GetTotalValue(&tmpScope, attributeName, false, ship, &foundAbsoluteValue));
                                        }
                                }

				if (!foundAbsoluteValue)
                        	{
	                                tmpScope.type = T_ZONE;
	                                totalValue = safeaddL(totalValue, GetTotalValue(&tmpScope, attributeName, false, ship, &foundAbsoluteValue));
				}

				if (foundAbsoluteValue && hasAbsoluteValue) *hasAbsoluteValue = true;
                                return totalValue;

                        default: //not supported
                                assertlm(false);

                                return 0;
                }
        }

        assertlm(false); //This point should be unreachable

        return 0;
}

static helptext_t attr_help =
        "Targets: Arena, Freq, Player\n"
        "Args: [{-z}] attribute name=value\n"
        "Gets or Sets the value of an attribute for the target\n"
        "if the = is ommited, the total value for the target is shown\n"
        "The value can be =123 (double ==), +123, or -123\n";
static void Cattr(const char *tc, const char *params, Player *p, const Target *target)
{
        Target scope = *target;

        const char *s;
        char attribute[256];
	long value = 0;
	int ship;
	ATTRIBUTESTRING_OPERATION operation;


        s = params;

        if (!p->arena)
                return;


        if (!(scope.type == T_PLAYER || scope.type == T_FREQ || scope.type == T_ARENA || scope.type == T_ZONE))
                return;

        if (parseAttributeString(s, attribute, 256, &value, &ship, &operation))
        {
        	Lock();
        	if (operation == ATTRMAN_OP_NOTHING)
        	{
	        	value = GetTotalValue(&scope, attribute, true, ship, NULL);
	        	chat->SendCmdMessage(p, "%s=%ld", s, value);
        	}
        	else if (operation == ATTRMAN_OP_UNSET)
        	{
        		UnsetValue(&scope, cmdSetter, attribute, ship);
        	}
        	else if (operation == ATTRMAN_OP_ABSOLUTE)
        	{
        		SetValue(&scope, cmdSetter, attribute, false, ship, value);
        	}
        	else if (operation == ATTRMAN_OP_ADDITION)
        	{
        		SetValue(&scope, cmdSetter, attribute, true, ship, value);
        	}
        	UnLock();
        }
        else
        {
        	chat->SendCmdMessage(p, "Incorrect syntax");
        }
}

// does not require lock
static bool parseAttributeString(const char *str, char *attribute, int maxAttributeSize, long *value, int *ship, ATTRIBUTESTRING_OPERATION *operation)
{
	char buf[256];
	char *start;
	char *end;
	char *hashSign;
        char *equalSign;
        char *s;
        int a;


        astrncpy(buf, str, sizeof(buf));
        hashSign = strchr(buf, '#');
        equalSign = strchr(buf, '=');

        *value = 0;
        *operation = ATTRMAN_OP_ERROR;
	*ship = ATTRMAN_NO_SHIP;


	if (hashSign)
        {
        	*hashSign = 0; //set the value of the hashSign to 0, we now have 2 strings
        	s = hashSign-1;


        	while (*s == ' ' && s >= buf) // trim spaces from the end
        	{
        		*s = 0;
        		s--;
        	}

        	s = buf;
        	while (*s == ' ' && *s) // up s until we find a character that is not a space
			s++;


		if (*s >= '0' && *s <= '9') //ship number
		{
			*ship = strtol(s, NULL, 0)-1;
			if (*ship < 0)
				*ship = ATTRMAN_NO_SHIP;
		}
		else if (*s) // havent reached the end of the string
		{
			*ship = ATTRMAN_NO_SHIP;
			for(a = 0; a < SHIPS; a++)
			{
				if (!stricmp(shipNames[a], s))
				{
					*ship = a;
					break;
				}
			}
		}
        }

        if (equalSign)
        {
		start = hashSign ? hashSign+1 : buf;
	        *equalSign = 0;

		s = equalSign-1;
		while (*s && *s == ' ' && s >= start) // trim spaces from the end
		{
			*s = 0; // end the string
			s--;
		}

	        s = start;
		while (*s == ' ' && *s) // up s until we find a character that is not a space
			s++;

		if (attribute)
		{
			astrncpy(attribute, s, maxAttributeSize);
			attribute[maxAttributeSize-1] = 0;
		}

		s = equalSign + 1;
		if (*s) //There is a value
		{
			*operation = ATTRMAN_OP_ADDITION;
			while ((*s == ' ' || *s == '=') && *s) //up s until we find a character that is not a space or equal sign
			{
				if (*s == '=') //found another =, so this is an absolute value not an addition
					*operation = ATTRMAN_OP_ABSOLUTE;
				s++;
			}

			if ((*s >= '0' && *s <= '9') || *s == '-' || *s == '+') //found a number
			{
				*value = strtol(s, NULL, 0);
			}
			else if (!*s) // end of string
			{
				*operation = ATTRMAN_OP_UNSET;
			}
			else // weird character
			{
				return false;
			}

		}
		else
		{
			*value = 0;
			*operation = ATTRMAN_OP_UNSET;
		}


		return true;
	}
	else
	{
		start = hashSign ? hashSign+1: buf;

		while (*start == ' ' && *start) // up start until we find a character that is not a space
			start++;

		s = strchr(start, 0)-1;
		end = s+1;
		while (*s == ' ' && s >= start) // trim spaces from the end
		{
			*s = 0;
			end = s;
			s--;
		}
		if (start >= end)
			return false;

		if (attribute) astrncpy(attribute, start, maxAttributeSize);
		*operation = ATTRMAN_OP_NOTHING;

		return true;
	}
}


EXPORT const char info_attrman[] = "Attribute Manager (" ASSSVERSION ", " BUILDDATE ") by JoWie <jowie@welcome-to-the-machine.com>\n";
static void ReleaseInterfaces()
{
        mm->ReleaseInterface(lm);
        mm->ReleaseInterface(pd);
        mm->ReleaseInterface(aman);
        mm->ReleaseInterface(cmd);
        mm->ReleaseInterface(chat);
        mm->ReleaseInterface(cfg);
}

EXPORT int MM_attrman(int action, Imodman *mm_, Arena *arena_)
{
        Link *link;
        Arena *arena;
        Player *p;
        pthread_mutexattr_t attr;

        if (action == MM_LOAD)
        {
                mm   = mm_;
                lm   = mm->GetInterface(I_LOGMAN,       ALLARENAS); //optional
                pd   = mm->GetInterface(I_PLAYERDATA,   ALLARENAS);
                aman = mm->GetInterface(I_ARENAMAN,     ALLARENAS);
                cmd  = mm->GetInterface(I_CMDMAN,       ALLARENAS);
                chat = mm->GetInterface(I_CHAT,         ALLARENAS);
                cfg  = mm->GetInterface(I_CONFIG,       ALLARENAS);

                if (!pd || !aman || !cmd || !chat || !cfg)
                {
                        printf("<attrman> Missing Interface\n");

                        ReleaseInterfaces();

                        return MM_FAIL;
                }

                arenaKey = aman->AllocateArenaData(sizeof(struct arenaData));
                if (arenaKey == -1)
                {
                        ReleaseInterfaces();
                        return MM_FAIL;
                }

                playerKey = pd->AllocatePlayerData(sizeof(struct playerData));
                if (playerKey == -1)
                {
                        aman->FreeArenaData(arenaKey);
                        ReleaseInterfaces();
                        return MM_FAIL;
                }


                zoneAttributes = HashAlloc();
                setValueCallbacks = HashAlloc();

                defaultZoneConfigSetter = RegisterSetter();
                defaultArenaConfigSetter = RegisterSetter();

		mm->RegCallback(CB_GLOBALCONFIGCHANGED, GlobalConfigChanged, ALLARENAS);
                mm->RegCallback(CB_NEWPLAYER, NewPlayer, ALLARENAS);
                mm->RegCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);

		pthread_mutexattr_init(&attr);
	        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE); // so that advisers may use interface methods
	        pthread_mutex_init(&mtx, &attr);
	        pthread_mutexattr_destroy(&attr);

                mm->RegInterface(&attrman, ALLARENAS);

                cmd->AddCommand("at", Cattr, ALLARENAS, attr_help);
                cmd->AddCommand("attr", Cattr, ALLARENAS, attr_help);

		Lock();
		ReadZoneSettings();
		UnLock();

                return MM_OK;
        }
        else if (action == MM_UNLOAD)
        {
                if (mm->UnregInterface(&attrman, ALLARENAS))
                        return MM_FAIL;

                cmd->RemoveCommand("at", Cattr, ALLARENAS);
                cmd->RemoveCommand("attr", Cattr, ALLARENAS);

		mm->UnregCallback(CB_GLOBALCONFIGCHANGED, GlobalConfigChanged, ALLARENAS);
                mm->UnregCallback(CB_NEWPLAYER, NewPlayer, ALLARENAS);
                mm->UnregCallback(CB_ARENAACTION, ArenaAction, ALLARENAS);

                UnregisterSetter(defaultZoneConfigSetter);
                UnregisterSetter(defaultArenaConfigSetter);

                afree(freeSetters);
                freeSetters_allocated = 0;
                freeSetters_count = 0;

		HashEnum(setValueCallbacks, hash_enum_afree, NULL);
                HashFree(setValueCallbacks);
                setValueCallbacks = HashAlloc();

                FreeZoneData(0);

                aman->Lock();
                FOR_EACH_ARENA(arena)
                {
                        FreeArenaData(GetArenaData(arena),0);
                }
                aman->Unlock();

                pd->Lock();
                FOR_EACH_PLAYER(p)
                {
                        FreePlayerData(GetPlayerData(p),0);
                }
                pd->Unlock();

		HashFree(setValueCallbacks);



                aman->FreeArenaData(arenaKey);
                pd->FreePlayerData(playerKey);
                ReleaseInterfaces();
                pthread_mutex_destroy(&mtx);
                return MM_OK;
        }

        return MM_FAIL;
}
